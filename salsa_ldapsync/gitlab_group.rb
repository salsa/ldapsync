module SalsaLdapsync
  class GitlabGroupMember
    attr_reader :id, :username

    def initialize(client, info)
      @client = client
      @id = info.id
      @username = info.username
    end
  end

  class GitlabGroupMembers
    PAGE_COUNT = 100  # XXX: 10000 users should be enough
    PAGE_SIZE = 100

    def initialize(client, id)
      @client = client

      @id = id
      @members = {}

      (1..PAGE_COUNT).each do |page|
        members = client.group_members(id, {
          :per_page => PAGE_SIZE,
          :page => page,
          :order_by => 'id',
          :sort => 'asc',
        })
        if members.length == 0
          break
        end
        $log.debug("Read Gitlab group members page #{page}")
        members.each do |user|
          @members[user.username] = GitlabGroupMember.new @client, user
        end
      end
    end

    def [](username)
      @members[username]
    end

    def add(user)
      $log.info("Adding user #{user.username} to group debian")
      member = @client.add_group_member(@id, user.id, 40)
      @members[user.username] = member
    end

    def delete(user)
      $log.info("Removing user #{user.username} from group debian")
      @client.remove_group_member(@id, user.id)
      @members.delete(user.username)
    end

    def has_key?(username)
      @members.has_key?(username)
    end
  end
end
