require_relative 'sshkey'

module SalsaLdapsync
  class LdapUser
    attr_reader :username, :name, :sshkeys

    def self.shadowexpire_now
      Time.now.to_i / (24*60*60)
    end

    def initialize(client, ldap_user)
      @client = client
      @username = ldap_user.uid[0]
      @name = "#{ldap_user.cn[0]} #{ldap_user.sn[0]}"

      @state_shadowexpire = if ldap_user.respond_to?(:shadowexpire)
                              ldap_user.shadowExpire[0].to_i
                            else
                              LdapUser.shadowexpire_now
                            end

      @sshkeys = if ldap_user.respond_to?(:sshrsaauthkey)
                   ldap_user.sshrsaauthkey.map do |key|
                     begin
                       s = SshKey.from_string(key).freeze
                       s if not s.options
                     rescue ArgumentError => e
                       # TODO
                     end
                   end.compact
                 else
                   []
                 end.to_set
    end

    def state
      now_day = Time.now.to_i / (24*60*60)

      # block gitlab user if ldap account is expired
      if @state_shadowexpire < LdapUser.shadowexpire_now
        'blocked'
      else
        'active'
      end
    end
  end

  class LdapUsers
    extend Forwardable

    ATTRIBUTES = [
      'cn',
      'shadowexpire',
      'sn',
      'sshrsaauthkey',
      'uid',
    ]

    def_delegators :@users, :[], :has_key?, :each, :each_key, :each_value

    def initialize(client, base:, filter:)
      @client = client

      @users = {}

      client.search(:base => base, :filter => filter, :attributes => ATTRIBUTES).each do |user|
        u = LdapUser.new @client, user
        $log.debug("Read LDAP user #{u.username}")
        @users[u.username] = u
      end
    end
  end
end
